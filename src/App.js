import React  from 'react';
import './App.css';
import Reviews from 'containers';

class App extends React.Component {
  render() {
    return (
      <Reviews />
    );
  }
}

export default App;
