import React from 'react'
import styles from './ReviewTab.css';
import classNames from 'classnames';

const ReviewTab = ({ index, review, active, onTabPress }) => {
    const image = require(`data/images/${review.thumb}`);
    const tabActiveStyle = { backgroundColor: review.color, borderColor: 'transparent' }
    const tabStyle = { borderBottomColor: review.color }
    const categoryStyle = { color: review.color }

    return(
        <div className={styles.content} style={active === index ? tabActiveStyle : tabStyle} role='button' onClick={onTabPress}>
            <img src={image} alt={review.name} className={classNames([styles.photo, active === index ? styles.photo_active : null])} />
            <div className={styles.inner}>
                <div className={styles.category} style={active !== index ? categoryStyle : null}>{review.sport}</div>
                <div className={active === index ? styles.text_active : styles.text}>{review.name}</div>
                <div className={active === index ? styles.text_active : styles.text}>{review.surname}</div>
            </div>
        </div>
)};

export default ReviewTab;