import React from 'react'
import styles from './Label.css';
import logo from './images/goal.svg'

const Label = ({ onClick, title }) => (
    <span className={styles.container}>
        <img src={logo} alt='label' className={styles.icon} border='0' />
        <span className={styles.title}>Цель</span>
    </span>
);

export default Label;