import React from 'react'
import styles from './Button.css';

const Button = ({ onClick, title }) => (
    <button type="button" className={styles.button} onClick={onClick}>
        <span className={styles.title}>{title}</span>
    </button>
);

export default Button;