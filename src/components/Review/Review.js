import React from 'react'
import { Label, Button } from 'components';
import styles from './Review.css';
import classNames from 'classnames';

const Review = ({ reviews, active }) => {
    const image = require(`data/images/${reviews[active].image}`);

    return (
        <div className={styles.container}>
            <div className={classNames([styles.column, styles.left])}>
                <img src={image} alt={reviews[active].name} className={styles.photo} />
                <div className={styles.content}>
                    {reviews[active].isVideo && <div className={styles.video}></div>}
                    <div className={styles.name}>
                        <div className={styles.text}>{reviews[active].name}</div>
                        <div className={styles.text}>{reviews[active].surname}</div>
                    </div>
                </div>
            </div>
            <div className={classNames([styles.column, styles.right])} style={{ backgroundColor: reviews[active].color }}>
                <div className={styles.header}>
                    <Label />
                    <span className={styles.title}>{reviews[active].title}</span>
                </div>
                <div className={styles.preview}>{reviews[active].text}</div>
                <Button title='Читать целиком' onClick={() => console.log('READ MORE')} />
            </div>
        </div>
    );
}

export default Review;