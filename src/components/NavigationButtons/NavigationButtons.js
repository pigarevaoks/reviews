import React from 'react'
import styles from './NavigationButtons.css';
import classNames from 'classnames';

const NavigationButtons = ({ onLeftBtnPress, onRightBtnPress, layout }) => (
    <div className={classNames([styles.container, layout ])}>
        <button className={classNames([styles.button, styles.left])} type="button" onClick={onLeftBtnPress}></button>
        <button className={classNames([styles.button, styles.right])} type="button" onClick={onRightBtnPress}></button>
    </div>
);

export default NavigationButtons;