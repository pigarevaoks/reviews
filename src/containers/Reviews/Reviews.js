import React from 'react';
import { NavigationButtons, Review, ReviewTab }  from 'components';
import styles from './Reviews.css';
import data from 'data/reviews.json'

class Reviews extends React.Component {

    state = { active: 0 }

    _onTabPress = value => {
        this.setState({ active: value })
    }

    _onTabPress = value => {
        this.setState({ active: value })
    }

    _onLeftBtnPress = () => {
        this.setState((prevState) => {
            if (prevState.active === 0) return { active: data.reviews.length - 1 };
            return { active: prevState.active - 1 };
        });
    }

    _onRightBtnPress = () => {
        this.setState((prevState) => {
            if (prevState.active === data.reviews.length - 1) return { active: 0 }
            return { active: prevState.active + 1 };
        });
    }

    render() {
        const { reviews } = data;
        const { active } = this.state;

        return (
            <div className={styles.grid}>
                <div className={styles.container}>
                    <header className={styles.header}>
                        <h1 className={styles.title}>Отзывы</h1>
                        <NavigationButtons layout={styles.buttons} onLeftBtnPress={this._onLeftBtnPress} onRightBtnPress={this._onRightBtnPress} />
                    </header>
                    <div className={styles.review}>
                        <Review reviews={reviews} active={active} />
                        <div className={styles.tabs}>
                            {reviews.map((review, index) => <ReviewTab key={index} review={review} active={active} index={index} onTabPress={() => this._onTabPress(index)} />)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Reviews;